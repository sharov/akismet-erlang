%% vim: ts=4 sts=4 sw=4 expandtab:
%% Erlang Akismet Library
%%
%% Copyright (c) 2008 Oleg Sharov, oleg@sharov.name
%% All rights reserved.
%%
%% Redistribution and use in source and binary forms, with or without
%% modification, are permitted provided that the following conditions are met:
%% * Redistributions of source code must retain the above copyright
%%   notice, this list of conditions and the following disclaimer.
%% * Redistributions in binary form must reproduce the above copyright
%%   notice, this list of conditions and the following disclaimer in the
%%   documentation and/or other materials provided with the distribution.
%%
%% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%% AND AN EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%% POSSIBILITY OF SUCH DAMAGE.

-module(akismet).

-author("Oleg Sharov <oleg@sharov.name>").
-version("0.2").
-define(CLIENT_NAME, "ErlangAkismetClient/0.1").

-export([
    check/6,
    ham/6,
    spam/6,
    verify_key/2,
    selftest/0
]).

verify_key(Key, Blog) ->
    Args = [{"blog", Blog}, {"key", Key}],
    ({ok, "valid"} == http_post(akismetUrl("verify-key"), Args)).

check(Key, Blog, UserIP, UserAgent, Content, Extra) ->
    case action("comment-check", Key, Blog, UserIP, UserAgent, Content, Extra) of
        {ok, "true"} -> spam;
        {ok, "false"} -> notspam;
        Error -> Error
    end.

spam(Key, Blog, UserIP, UserAgent, Content, Extra) ->
    case action("submit-spam", Key, Blog, UserIP, UserAgent, Content, Extra) of
        {ok, _} -> ok;
        Error -> Error
    end.

ham(Key, Blog, UserIP, UserAgent, Content, Extra) ->
    case action("submit-ham", Key, Blog, UserIP, UserAgent, Content, Extra) of
        {ok, _} -> ok;
        Error -> Error
    end.

action(Action, Key, Blog, UserIP, UserAgent, Content, Extra) ->
    Args = akismet_params(Blog, UserIP, UserAgent, Content, Extra),
    Url = actionUrl(Key, Action),
    http_post(Url, Args).

actionUrl(Key, Action) -> akismetUrl(Key ++ ".", Action).

akismetUrl(Action) -> akismetUrl("", Action).
akismetUrl(SubDomain, Action) ->
    "http://"++ SubDomain ++"rest.akismet.com/1.1/"++ Action.

http_post(Url, Args) ->
    HTTPResult = httpc:request(post, {
        Url,
        [{"User-Agent", ?CLIENT_NAME}],
        "application/x-www-form-urlencoded; charset=\"utf-8\"",
        params(Args)
    } , [{timeout, 1000}], []),
    case HTTPResult of
        {ok, {_, _, Res}} -> {ok, Res};
        {error, _} = Error -> Error
    end.

akismet_params(Blog, UserIP, UserAgent, Content, UserExtra) ->
    [
        {"blog", Blog},
        {"user_ip", UserIP},
        {"user_agent", UserAgent},
        {"comment_content", Content}
        | [
            {K, proplists:get_value(K, UserExtra, "")} || K <-
            ["referrer", "permalink", "comment_type", "comment_author",
                "comment_author_email", "comment_author_url"]
        ]
    ].

params([]) -> [];
params(KV) -> params(KV, []).

params([{K, V} | Tail], Acc) ->
    NewAcc = [K, $=, url_encode(V), $& | Acc],
    case Tail of
        [] -> lists:flatten(NewAcc);
        _ -> params(Tail, NewAcc)
    end.

%%%% copied from yaws_api.erl
url_encode([H|T]) when is_list(H) ->
    [url_encode(H) | url_encode(T)];
url_encode([H|T]) ->
    if
        H >= $a, $z >= H ->
            [H|url_encode(T)];
        H >= $A, $Z >= H ->
            [H|url_encode(T)];
        H >= $0, $9 >= H ->
            [H|url_encode(T)];
        H == $_; H == $.; H == $-; H == $/; H == $: -> % FIXME: more..
            [H|url_encode(T)];
        true ->
            case integer_to_hex(H) of
                [X, Y] ->
                    [$%, X, Y | url_encode(T)];
                [X] ->
                    [$%, $0, X | url_encode(T)]
            end
     end;
url_encode([]) -> [].

integer_to_hex(I) ->
    case catch erlang:integer_to_list(I, 16) of
        {'EXIT', _} -> old_integer_to_hex(I);
        Int         -> Int
    end.

old_integer_to_hex(I) when I < 10 ->
    integer_to_list(I);
old_integer_to_hex(I) when I < 16 ->
    [I-10+$A];
old_integer_to_hex(I) when I >= 16 ->
    N = trunc(I/16),
    old_integer_to_hex(N) ++ old_integer_to_hex(I rem 16).
%%%% /end

selftest() ->
    Key = "valid_key_here",

    Blog = "http://example.com/test.html",
    UserIP = "85.58.33.17",
    UserAgent = "Mozilla/5.0",
    Content = "ohoho",
    Extra = [{"referrer", "http://lola.home/"},
        {"permalink", ""},
        {"comment_type", ""},
        {"comment_author", "batan"},
        {"comment_author_email", ""},
        {"comment_author_url", ""}],

    verify_key(Key, "http://example.com/"),

    notspam = check(Key, Blog, UserIP, UserAgent, Content, [{"comment_author", "batan"} | Extra]),

    spam = check(Key, Blog, UserIP, UserAgent, Content, [{"comment_author", "viagra-test-123"} | Extra]),

    ok.
